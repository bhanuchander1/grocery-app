<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Route::get('/', function ()
//     return view('welcome');
// });
Route::get('auth/login', 'Auth\AuthController@login');
Route::post('auth/login', 'Auth\AuthController@login');
Route::get('auth/logout', 'Auth\AuthController@getLogout');
// Route::get('auth/register', 'Auth\AuthController@getRegister');
// Route::post('/register', 'Auth\AuthController@postRegister');
Route::get('main','groceryappController@index');

Route::get('/cart','OrderController@checkout');
Route::post('/order/store','OrderController@store');
Route::get('/addtocart/{id}','OrderController@addToCart');
