@extends('app')
@section('message')
  <div class="row text-center">
    @foreach($productvar as $prod)
      <div class="col-md-3 col-sm-6 hero-feature">
            <div class="thumbnail">
<img src="/img/{{$prod->image}}" alt="">
      <div class="caption">
          <h3>{{$prod->name}}</h3>
        <p>{{$prod->description}}</p>
    <p><span class="h3">&#8377;{{$prod->amount}}</span> <br>
<a href="/addtocart/{{$prod->id}}" class="btn btn-primary btn-block {{in_array($prod,session('checkout.cart',array())) ? 'disabled' : ''}}">Add to cart</a></p>
            </div>
            </div>
        </div>
    @endforeach
      </div>
  @endsection
